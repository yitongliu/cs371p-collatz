// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------


#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <map>
#include <vector>

#include "Collatz.h"

using namespace std;

// eager cache stores the cycle length of a number
map<int, int> eager_cache;
// the largest number stored in eager cache
const int kMax = 5000;
// meta cache stores numbers whose cycle length is larger than all smaller numbers
vector<int> meta_cache {1, 2, 3, 6, 7, 9, 18, 25, 27, 54, 73, 97, 129, 171, 231,
           313, 327, 649, 703, 871, 1161, 2223, 2463, 2919, 3711,
           6171, 10971, 13255, 17647, 23529, 26623, 34239, 35655,
           52527, 77031, 106239, 142587, 156159, 216367, 230631,
           410011, 511935, 626331, 837799
};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

int cycle_length (int num_copy) {
    long long num = num_copy;
    assert (num > 0);
    int cnt = 1;
    while (num != 1) {
        if (num % 2 == 0) {
            num /= 2;
        } else {
            // find if the number is in eager cache
            if (num < kMax && eager_cache[num] != 0)
                return cnt - 1 + eager_cache[num];
            num = num * 3 + 1;
        }
        cnt++;
    }
    assert (cnt > 0 && num == 1);
    return cnt;
}

void build_eager_cache () {
    if (eager_cache.empty()) {
        for (int num = 1; num <= kMax; num += 2)
            eager_cache[num] = cycle_length(num);
    }
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    build_eager_cache();
    assert (i > 0 && j > 0 && i < 1000000 && j < 1000000);
    if (i > j) {
        // swap i and j
        i = i + j;
        j = i - j;
        i = i - j;
    }

    // check if meta cache has the answer
    unsigned int idx = 0;
    while (idx < meta_cache.size() && meta_cache[idx] <= j)
        idx += 1;
    idx -= 1;
    if (meta_cache[idx] >= i)
        return cycle_length(meta_cache[idx]);

    // otherwise use simple method to compute
    int ans = 0;
    for (int num = i; num <= j; num++) {
        int res = cycle_length(num);
        ans = max(ans, res);
    }
    assert (ans > 0);
    return ans;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
